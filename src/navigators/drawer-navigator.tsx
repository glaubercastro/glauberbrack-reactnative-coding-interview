import { createDrawerNavigator } from '@react-navigation/drawer';
import { Profile } from '../screens';
import { MainNavigator } from './index';

const Drawer = createDrawerNavigator();

export const DrawerNavigator = () => {
  return (
    <Drawer.Navigator>
      <Drawer.Screen name="Main" component={MainNavigator} />
      <Drawer.Screen name="Profile" component={Profile} />
    </Drawer.Navigator>
  );
};
