import React from 'react';
import styles from './styles';
import { View, Text, SafeAreaView } from 'react-native';

export const Profile = () => {
  return (
    <SafeAreaView style={styles.wrapper}>
      <Text style={styles.title}>Profile</Text>
    </SafeAreaView>
  );
};
